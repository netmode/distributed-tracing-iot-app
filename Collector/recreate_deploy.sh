sudo docker build --network=host -t iot-collector:latest .
sudo docker tag iot-collector:latest $1/iot-collector:latest
sudo docker push $1/iot-collector:latest
kubectl apply -f yaml/iot-collector-configmap.yaml
kubectl delete deploy iot-collector
kubectl apply -f yaml/iot-collector-deployment.yaml
