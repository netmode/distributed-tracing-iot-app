from flask import Flask, request
import requests
from py_zipkin.zipkin import zipkin_span, create_http_headers_for_new_span, ZipkinAttrs, Kind, zipkin_client_span
from py_zipkin.request_helpers import create_http_headers
from py_zipkin.encoding import Encoding
import dateutil.parser as dp
import json
import random
from datetime import datetime
from prometheus_client import start_http_server, Summary
#from prometheus_flask_exporter import PrometheusMetrics
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from prometheus_client import make_wsgi_app
from prometheus_client import Counter
import os
#from app import app
import time

SENSOR_NAME = os.getenv('SENSOR', 'test')
PARAMETER_NAME = os.getenv('PARAMETER', 'test')

app = Flask(__name__)
#metrics = PrometheusMetrics(app)
#REQUEST_TIME = Summary('request_processing_seconds', 'Time spent processing request')
# Add prometheus wsgi middleware to route /metrics requests
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
    '/metrics': make_wsgi_app()
})

c = Counter('http_requests_total', 'HTTP request count', ['method', 'endpoint'])
bytes_sent = Counter('bytes_sent_total', 'Bytes sent for preprocessing', ['method', 'endpoint'])
latency = Counter('request_latency_seconds_total', 'Collection seconds total', ['method', 'endpoint'])
errors = Counter('errors_total', 'Errors total', ['method', 'endpoint'])

#from prometheus_client import multiprocess
#from prometheus_client import CollectorRegistry
#registry = CollectorRegistry()
#multiprocess.MultiProcessCollector(registry)

def default_handler(encoded_span):
    body = encoded_span

    # decoded = _V1ThriftDecoder.decode_spans(encoded_span)
    app.logger.debug("body %s", body)

    # return requests.post(
    #     "http://zipkin:9411/api/v1/spans",
    #     data=body,
    #     headers={'Content-Type': 'application/x-thrift'},
    # )

    return requests.post(
            "http://"+os.getenv('ZIPKIN_HOST', "192.168.2.28")+":"+os.getenv('ZIPKIN_PORT', "9411")+"/api/v2/spans",
        data=body,
        headers={'Content-Type': 'application/json'},
    )

@zipkin_client_span(service_name='iot-collector', span_name='get_value')
def get_value():
    waqi_token = '12eb788d1022dcc7f7f04c7219f21eec3fcbb9e2'
    query = {'token': waqi_token}
    response = requests.get('http://api.waqi.info/feed/athens/', params=query)
    return response.json()

@zipkin_client_span(service_name='iot-collector', span_name='send_value')
def send_value(headers,post_json):
    url = 'http://'+os.getenv('PREPROCESSOR_HOST', '192.168.2.28')+':'+os.getenv('PREPROCESSOR_PORT', '5001')+'/preprocess'
    headers = create_http_headers()
    headers['Content-Type'] = 'application/json'
    print(headers)
    print("Sending value to preprocessor...")
    response = requests.post(url, data = post_json, headers=headers)
    print("Value sent to preprocessor!")
    bytes_sent.labels('post', '/preprocess').inc(len(post_json),exemplar={'trace_id': headers['X-B3-TraceId']})
    return 'OK'

@zipkin_span(service_name='iot-collector', span_name='get_value')
def get_test_value():
    #headers = create_http_headers()
    #print(headers)
    #with zipkin_span(
    #    service_name='iot-collector',
    #    span_name='get_value',
    #    zipkin_attrs=ZipkinAttrs(
    #        trace_id=headers['X-B3-TraceId'],
    #        span_id=headers['X-B3-SpanId'],
    #        parent_span_id=headers['X-B3-ParentSpanId'],
    #        flags=1,
    #        is_sampled=headers['X-B3-Sampled'],
    #    ),
    #    transport_handler=default_handler,
    #    port=int(os.getenv('COLLECTOR_PORT', '5000')),
    #    sample_rate=100,
    #) as zipkin_context:
    value = random.randint(0,150)
    timestamp = str(datetime.now())
        #zipkin_context.update_binary_annotations({'remoteEndpoint': {'serviceName': 'AQI_API', 'ipv4': 'api.waqi.info', 'port': 80}})
        #zipkin_context.add_sa_binary_annotation(
        #    port=80,
        #    service_name='AQI_API',
        #    host='api.waqi.info',
        #)
    return {"data": {"aqi": value, "time": {"iso": timestamp } } }

@app.route('/')
def index():
    print("Index function is starting...")
    with zipkin_span(
        service_name='iot-collector',
        span_name='collection',
        transport_handler=default_handler,
        port=int(os.getenv('COLLECTOR_PORT', "5000")),
        sample_rate=100,
        encoding=Encoding.V2_JSON
    ):
        #as zipkin_context:
        #zipkin_context.update_binary_annotations({'remoteEndpoint': {'serviceName': 'AQI_API', 'ipv4': 'api.waqi.info', 'port': 80}})
        #zipkin_context.add_sa_binary_annotation(
        #    port=80,
        #    service_name='AQI_API',
        #    host='api.waqi.info',
        #)
        start = time.time()
        print("Trace initialized...")
        headers = create_http_headers()
        print(headers)
        c.labels('get', '/').inc(exemplar={'trace_id': headers['X-B3-TraceId']})
        received_json = get_test_value()
        print(received_json)

        aqi_value = received_json["data"]["aqi"]
        aqi_timestamp = received_json["data"]["time"]["iso"]

        post_json = { "sensor": SENSOR_NAME, "parameter": PARAMETER_NAME, "value": aqi_value, "timestamp": aqi_timestamp }
        try:
            response = send_value(headers,json.dumps(post_json))
            print(response)
        except Exception as e:
            print(e)
            errors.labels('get', '/').inc(exemplar={'trace_id': headers['X-B3-TraceId']})

        end = time.time()
        print("Duration: "+str(end-start))
        latency.labels('get', '/').inc(end-start,exemplar={'trace_id': headers['X-B3-TraceId']})
    return 'OK', 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', threaded=True)
