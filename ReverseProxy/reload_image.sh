sudo docker build --network=host -t reverse-proxy:latest .
sudo docker tag reverse-proxy:latest gtzanet/reverse-proxy:latest
sudo docker push gtzanet/reverse-proxy:latest
kubectl apply -f yaml/reverse-proxy-configmap.yaml
kubectl delete deploy reverse-proxy
kubectl apply -f yaml/reverse-proxy-deployment.yaml
