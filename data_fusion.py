import requests
import json
import pandas as pd
from numpy import mean




#from timestamp in microseconds(zipkin) to prometheus format (in secs.millisecs)
def prom_timestamp(timestamp):
    t = str(timestamp)[:13]
    t = t[:10]+'.'+t[10:]
    return t 

def cpuValues (svc,start,end ):
    start = prom_timestamp(start) #msec string
    end = prom_timestamp(end) #msec string 
    step = 1 #1 ms resolution 
    while (float(end)-float(start))*1000/step > 11000 : #no to exceed maximum resolution of 11,000 points per timeseries
        step *=10
    cpu = requests.get("http://10.0.2.36:30867/api/v1/query_range?query=rate(container_cpu_usage_seconds_total{container=%22"+svc+"%22}[1m])&start="+start+"&end="+end+"&step="+str(step)+"ms").json()
    if(cpu["data"]["result"] == []):
        print(svc , "cpu returned []")
        return []
    else:
        return cpu["data"]["result"][0]["values"] #list of lists  

def memValues(svc,start,end ):
    start = prom_timestamp(start)
    end = prom_timestamp(end)
    step = 1 #1ms resolution
    while (float(end)-float(start))*1000/step > 11000 : #no to exceed maximum resolution of 11,000 points per timeseries
        step *=10
    mem = requests.get("http://10.0.2.36:30867/api/v1/query_range?query=container_memory_working_set_bytes{container=%22"+svc+"%22}&start="+start+"&end="+end+"&step="+str(step)+"ms").json()
    if(mem["data"]["result"] == []):
        print(svc, "memory returned []")
        return []
    else:
        return mem["data"]["result"][0]["values"] #list of lists  

def throughput(start,end):
    start = prom_timestamp(start)
    end = prom_timestamp(end)
    step = 1 # 1ms resoluton 
    while (float(end)-float(start))*1000/step > 11000 : #no to exceed maximum resolution of 11,000 points per timeseries
        step *=10
    app = "iot-preprocessor"
    thr = requests.get("http://10.0.2.36:30867/api/v1/query_range?query=rate(http_requests_total{app=%22"+app+"%22}[10s])&start="+start+"&end="+end+"&step="+str(step)+"ms").json()
    return thr["data"]["result"][0]["values"]

    
def spanInfo (id,name,timestamp,duration,error,throughput, services , cpu_values,mem_values) :
    metrics =[]
    s = 0 
    for pod in services:
        cpu = mean([float(i[1]) for i in cpu_values[s]])
        mem = mean([float(i[1]) for i in mem_values[s]]) 
        metrics.append(pods_metrics_avg(pod,cpu,mem))
        s +=1
    return {
        "span_id":id,
        "name":name,
        "timestamp(micro)":timestamp,
        "span_duration(micro)":duration,
        "error": error,
        "throughput_avg":mean([float(i[1]) for i in throughput]),
        "pods_metrics_avg":metrics
    }

def pods_metrics_avg (svc,cpu,memory):
    return { 
        "pod":svc,
        "cpu":cpu,
        "memory":memory
    }
def fusion(response,mylist):
    data = response["data"] #list of exemplars dictionaries
    for dic in data:
        if(dic["seriesLabels"]["app"] == "iot-collector"):
            for ex in dic["exemplars"]:
                correlation = {}
                traceID = ex["labels"]["trace_id"] #get traceid from exemplars
                zipkin = requests.get("http://10.0.2.36:9411/api/v2/trace/"+traceID) #search on zipkin this trace id 
                if(zipkin.status_code == 404 ):
                    print("zipkin trace not found ", traceID)
                    continue
                zipkin = sorted(zipkin.json(), key=lambda k: k["timestamp"], reverse=True) #sort spans based on timestamp 
                #print(json.dumps(zipkin,indent=4))
                correlation["traceID"] = traceID
                correlation["timestamp_end(sec)"] = ex["timestamp"]
                correlation["latency(sec)"] =  ex["value"]
                correlation["root_span"] = {}
                correlation["spans"] = [] 
                trace_services = set() 
                
                for z in zipkin : #for every span
                    cpu_values =[]
                    mem_values = []
                    services = []
                    start = z['timestamp']
                    end = start + z['duration']
                    error = None
                    if( 'tags' in z) and ('error' in z['tags']):
                        error = z['tags']['error']
                    services.append(z['localEndpoint']['serviceName'])
                    if 'parentId' in z: #not the root span 
                        if ('shared' in z) and (z['shared']== True ): #remote call (server side) 
                            remote_service = z['localEndpoint']['serviceName'] # to pass to the client side 
                        if ('kind' in z) and (z['kind'] == 'CLIENT'):
                            services.append(remote_service)
                        trace_services.update(services)
                        for s in services:
                            cpu_values.append(cpuValues(s,start,end)) #list of lists
                            mem_values.append(memValues(s,start,end))  
                        correlation["spans"].append(spanInfo(z['id'],z['name'],start,z['duration'],error,throughput(start,end),services,cpu_values,mem_values))                 
                    else: #root span
                        trace_services.update(services)
                        services = trace_services
                        for s in services:
                            cpu_values.append(cpuValues(s,start,end)) #list of lists
                            mem_values.append(memValues(s,start,end))
                        correlation["root_span"] = spanInfo(z['id'],z['name'],start,z['duration'],error,throughput(start,end),services, cpu_values,mem_values)
                correlation["spans"] = sorted(correlation["spans"], key=lambda k: k["timestamp(micro)"])
                json_dump = json.dumps(correlation, indent=4)
                print(json_dump)
                mylist.append(correlation)
    return mylist

exemplars = ['request_latency_seconds_total']
mylist = []
for query in exemplars :
    response = requests.get("http://10.0.2.36:30867/api/v1/query_exemplars?query="+query+"&start=1643969540.235&end=1643969720.235").json() #&start=1632334294.398&end=1625315099.029     
    mylist = fusion(response,mylist)



mylist = sorted(mylist,key = lambda k: k["timestamp_end(sec)"])
#json.dumps(mylist, indent=4)
#print(type(mylist))

with open('fusion.json','w') as f:
    json.dump(mylist,f)
