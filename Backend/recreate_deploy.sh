sudo docker build --network=host -t iot-backend:latest .
sudo docker tag iot-backend:latest $1/iot-backend:latest
sudo docker push $1/iot-backend:latest
kubectl apply -f yaml/iot-backend-configmap.yaml
kubectl delete deploy iot-backend
kubectl apply -f yaml/iot-backend-deployment.yaml
