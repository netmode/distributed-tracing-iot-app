import os
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from database.models import *
from database.serializers import *
from rest_framework.authtoken.views import ObtainAuthToken
from django.db.models import Q
import requests
from rest_framework.response import Response
from datetime import timedelta, date
from py_zipkin.zipkin import zipkin_span, create_http_headers_for_new_span, ZipkinAttrs, Kind, zipkin_client_span
from py_zipkin.request_helpers import create_http_headers
from py_zipkin.encoding import Encoding
from dateutil import parser

# Create your views here.

def default_handler(encoded_span):
    body = encoded_span
    return requests.post(
            'http://'+os.getenv('ZIPKIN_HOST', '192.168.2.28')+':'+os.getenv('ZIPKIN_PORT', '9411')+'/api/v2/spans',
        data=body,
        headers={'Content-Type': 'application/json'},
    )

class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    def create(self, request):
        try:
            data = request.data
            s = UserSerializer(data=data)
            print("Valid data: "+str(s.is_valid()))
            if s.is_valid():
                username = data['username']
                password = data['password']
                is_staff = data['is_staff']
                role = data['role']
                email = data['email']
                User.objects.create_user(username=username,email=email,password=password,is_staff=is_staff,role=role)
                return Response(status=201, data=data)
            else:
                data = s.errors
                return Response(status=400, data=data)
        except Exception as e:
            print(e)
            return HttpResponse(status=400)

class CustomObtainAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super(CustomObtainAuthToken, self).post(request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        print(token)
        role = token.user.role
        return Response({'token': token.key, 'role': role, 'id': token.user_id})

class ParameterViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Parameters to be CRUDed.
    """
    serializer_class = ParameterSerializer
    
    @zipkin_span(service_name='iot-backend', span_name='retrieve_parameter_info')
    def get_data(self,request):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, many=True)
        return serializer.data

    def list(self, request):
        with zipkin_span(
            service_name='iot-backend',
            zipkin_attrs=ZipkinAttrs(
                trace_id = request.META['HTTP_X_B3_TRACEID'],
                span_id = request.META['HTTP_X_B3_SPANID'],
                parent_span_id = request.META['HTTP_X_B3_PARENTSPANID'],
                flags=1,
                is_sampled = request.META['HTTP_X_B3_SAMPLED'],
            ),
            span_name='get_parameter_info',
            transport_handler=default_handler,
            port=8000,
            sample_rate=100,
            encoding=Encoding.V2_JSON
        ):
            data = self.get_data(request)
        return Response(status=200, data=data)

    def get_queryset(self):
        sensor = self.request.query_params.get('sensor', None)
        serial = self.request.query_params.get('serial', None)
        type = self.request.query_params.get('type', None)
        query = Q()
        if sensor is not None:
            query = query & Q(sensor=sensor)
        if serial is not None:
            query = query & Q(sensor__serial=serial)
        if type is not None:
            query = query & Q(type=type)
        #if not self.request.user.is_staff:
        #    query = query & Q(sensor__owner=self.request.user)    
        queryset = Parameter.objects.all()
        queryset = queryset.filter(query)
        return queryset

class SensorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Sensors to be CRUDed.
    """
    serializer_class = SensorSerializer

    def get_queryset(self):
        query = Q()
        queryset = Sensor.objects.all()
        queryset = queryset.filter(query)
        return queryset

class ValueViewSet(viewsets.ModelViewSet):
    serializer_class = ValueSerializer
    def get_queryset(self):
        parameter = self.request.query_params.get('parameter',None)
        starts_after = self.request.query_params.get('starts_after', None)
        ends_before = self.request.query_params.get('ends_before', None)
        last = self.request.query_params.get('last', 0)
        queryset = self.model.objects.all()
        query = Q()
        if parameter is not None:
            query = query & Q(parameter=parameter)
        if starts_after is not None:
            query = query & Q(timestamp__gte=starts_after)
        if ends_before is not None:
            query = query & Q(timestamp__lte=ends_before)
        queryset = queryset.filter(query)
        if last is not 0:
            sliced_qs = queryset.order_by('-timestamp')[:int(last)]
            queryset = queryset.filter(id__in=sliced_qs).order_by('timestamp')
        #if not self.request.user.is_staff:
        #    query = query & Q(parameter__sensor__owner=self.request.user)
        #print(queryset)
        return queryset

@zipkin_client_span(service_name='iot-backend', span_name='generate_prediction')
def predict(ID,date):
    url = 'http://'+os.getenv('PREDICTOR_HOST', '192.168.2.28')+':'+os.getenv('PREDICTOR_PORT', '9002')+'/predict?parameter='+str(ID)+'&start='+str(date)
    headers = create_http_headers() 
    response = requests.get(url, headers=headers)
    return response

class SensorvalueViewSet(ValueViewSet):
    """
    API endpoint that allows Sensorvalues to be CRUDed.
    """
    model = Sensorvalue
    
    def list(self, request):
        with zipkin_span(
            service_name='iot-backend',
            zipkin_attrs=ZipkinAttrs(
                trace_id = request.META['HTTP_X_B3_TRACEID'],
                span_id = request.META['HTTP_X_B3_SPANID'],
                parent_span_id = request.META['HTTP_X_B3_PARENTSPANID'],
                flags=1,
                is_sampled = request.META['HTTP_X_B3_SAMPLED'],
            ),
            span_name='get_data',
            transport_handler=default_handler,
            port=int(os.getenv('BACKEND_PORT', '8000')),
            sample_rate=100,
            encoding=Encoding.V2_JSON
        ):
            queryset = self.get_queryset()
            serializer = self.serializer_class(queryset, many=True)
            return Response(status=200, data=serializer.data)

    @zipkin_span(service_name='iot-backend', span_name='store_value')
    def create_value(self,request):
        return super().create(request)
    
    def create(self, request):
        with zipkin_span(
            service_name='iot-backend',
            zipkin_attrs=ZipkinAttrs(
                trace_id = request.META['HTTP_X_B3_TRACEID'],
                span_id = request.META['HTTP_X_B3_SPANID'],
                parent_span_id = request.META['HTTP_X_B3_PARENTSPANID'],
                flags=1,
                is_sampled = request.META['HTTP_X_B3_SAMPLED'],
            ),
            span_name='store_and_predict',
            transport_handler=default_handler,
            port=int(os.getenv('BACKEND_PORT', '8000')),
            sample_rate=100,
            encoding=Encoding.V2_JSON
        ):
            self.create_value(request)
            print(request.data["timestamp"])
            day = parser.parse(request.data["timestamp"]).date()
            print(day)
            return predict(request.data["parameter"],day+timedelta(days=1))

        #return Response(status=201, data=request.data)

class PredictionViewSet(ValueViewSet):
    """
    API endpoint that allows Predictions to be CRUDed.
    """
    model = Prediction

    def create(self, request):
        with zipkin_span(
            service_name='iot-backend',
            zipkin_attrs=ZipkinAttrs(
                trace_id = request.META['HTTP_X_B3_TRACEID'],
                span_id = request.META['HTTP_X_B3_SPANID'],
                parent_span_id = request.META['HTTP_X_B3_PARENTSPANID'],
                flags=1,
                is_sampled = request.META['HTTP_X_B3_SAMPLED'],
            ),
            span_name='store_prediction',
            transport_handler=default_handler,
            port=int(os.getenv('BACKEND_PORT', '8000')),
            sample_rate=100,
            encoding=Encoding.V2_JSON
        ):
            super().create(request)
        return Response(status=201, data=request.data)
