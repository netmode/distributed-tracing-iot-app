import requests
from py_zipkin.zipkin import zipkin_span, create_http_headers_for_new_span, ZipkinAttrs, Kind, zipkin_client_span
from py_zipkin.request_helpers import create_http_headers
from py_zipkin.encoding import Encoding
from time import sleep
from fastapi import FastAPI, Header
from fastapi.logger import logger
from pydantic import BaseModel
from typing import Optional
from starlette_exporter import PrometheusMiddleware, handle_metrics
from prometheus_client import Counter

app = FastAPI()
app.add_middleware(PrometheusMiddleware, app_name='Preprocessor')
app.add_route("/metrics", handle_metrics)

c = Counter('preprocess_requests', 'Total requests for preprocessing', ['method', 'endpoint'])

class Sensorvalue(BaseModel):
    sensor: str
    parameter: str
    value: float
    timestamp: str

def default_handler(encoded_span):
    body = encoded_span
    return requests.post(
        "http://192.168.2.28:9411/api/v2/spans",
        data=body,
        headers={'Content-Type': 'application/json'},
    )

@zipkin_client_span(service_name='preprocessor', span_name='analysis')
def analyse(value):
    return { "sensor": value.sensor, "parameter": value.parameter, "value": value.value, "timestamp": value.timestamp }

@zipkin_client_span(service_name='preprocessor', span_name='add_parameter_id')
def add_parameter_id(json):
    url = 'http://192.168.2.28:8000/api/parameters?serial='+json["sensor"]+'&type='+json["parameter"]
    django_token = '94e0a53b32b96a31c86d038f1d76bc445fc79bc3'
    headers = create_http_headers()
    headers['Authorization'] = 'Token ' + django_token
    response = requests.get(url, headers=headers)
    json["parameter"] = response.json()["data"][0]["id"]
    del json["sensor"]
    return json
    
@zipkin_client_span(service_name='preprocessor', span_name='store_value')
def store_value(post_json):
    url = 'http://192.168.2.28:8000/api/sensorvalues'
    django_token = '94e0a53b32b96a31c86d038f1d76bc445fc79bc3'
    headers = create_http_headers()
    headers['Authorization'] = 'Token ' + django_token
    print(headers)
    response = requests.post(url, data = post_json, headers=headers)
    #print(response.json())
    return 'OK'

@app.post("/preprocess")
async def preprocess_api(value: Sensorvalue, x_b3_traceid: Optional[str] = Header(None), x_b3_spanid: Optional[str] = Header(None), x_b3_parentspanid: Optional[str] = Header(None),x_b3_sampled: Optional[str] = Header(None)):
    with zipkin_span(
        service_name='preprocessor',
        zipkin_attrs=ZipkinAttrs(
            #trace_id=request.headers['X-B3-TraceID'],
            trace_id = x_b3_traceid,
            #span_id=request.headers['X-B3-SpanID'],
            span_id = x_b3_spanid,
            #parent_span_id=request.headers['X-B3-ParentSpanID'],
            parent_span_id = x_b3_parentspanid,
            flags=1,
            #is_sampled=request.headers['X-B3-Sampled'],
            is_sampled = x_b3_sampled,
        ),
        span_name='preprocessing',
        transport_handler=default_handler,
        port=5001,
        sample_rate=100,
        encoding=Encoding.V2_JSON
    ):
        c.labels('post', '/preprocess').inc(1,exemplar={'trace_id': x_b3_traceid})
        json = analyse(value)
        json = add_parameter_id(json)
        print(json)
        store_value(json)
    return 'OK', 200
