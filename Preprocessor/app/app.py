import os
import requests
from py_zipkin.zipkin import zipkin_span, create_http_headers_for_new_span, ZipkinAttrs, Kind, zipkin_client_span
from py_zipkin.request_helpers import create_http_headers
from py_zipkin.encoding import Encoding
from time import sleep
#from fastapi import FastAPI, Header
#from fastapi.logger import logger
from pydantic import BaseModel
from typing import Optional
#from starlette_exporter import PrometheusMiddleware, handle_metrics
from prometheus_client import Counter
from flask import Flask,request
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from prometheus_client import make_wsgi_app
import json
import time

app = Flask(__name__)
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
    '/metrics': make_wsgi_app()
})

c = Counter('http_requests_total', 'Total requests for preprocessing', ['method', 'endpoint'])
bytes_received = Counter('bytes_received', 'Total bytes received', ['method', 'endpoint'])
latency = Counter('request_latency_seconds_total', 'Preprocessing latency', ['method', 'endpoint'])
errors = Counter('errors_total', 'Preprocessing errors', ['method', 'endpoint'])

class Sensorvalue(BaseModel):
    sensor: str
    parameter: str
    value: float
    timestamp: str

def default_handler(encoded_span):
    body = encoded_span
    return requests.post(
        "http://"+os.getenv('ZIPKIN_HOST', '192.168.2.28')+':'+os.getenv('ZIPKIN_PORT', '9411')+'/api/v2/spans',
        data=body,
        headers={'Content-Type': 'application/json'},
    )

@zipkin_span(service_name='iot-preprocessor', span_name='analysis')
def analyse(data):
    sleep(1)
    return { "sensor": data["sensor"], "parameter": data["parameter"], "value": data["value"], "timestamp": data["timestamp"] }

@zipkin_client_span(service_name='iot-preprocessor', span_name='add_parameter_id')
def add_parameter_id(json_file):
    url = 'http://'+os.getenv('BACKEND_HOST', '192.168.2.28')+':'+os.getenv('BACKEND_PORT', '8000')+'/api/parameters?serial='+json_file["sensor"]+'&type='+json_file["parameter"]
    django_token = os.getenv('BACKEND_AUTH_TOKEN')
    headers = create_http_headers()
    if django_token:
        headers['Authorization'] = 'Token ' + django_token
    response = requests.get(url, headers=headers)
    print(len(json.dumps(response.json())))
    bytes_received.labels('get','api/parameters').inc(len(response.content),exemplar={'trace_id': headers['X-B3-TraceId']})
    json_file["parameter"] = response.json()["data"][0]["id"]
    del json_file["sensor"]
    return json_file
    
@zipkin_client_span(service_name='iot-preprocessor', span_name='store_value')
def store_value(post_json):
    url = 'http://'+os.getenv('BACKEND_HOST', '192.168.2.28')+':'+os.getenv('BACKEND_PORT', '8000')+'/api/sensorvalues'
    django_token = os.getenv('BACKEND_AUTH_TOKEN')
    headers = create_http_headers()
    if django_token:
        headers['Authorization'] = 'Token ' + django_token
    print(headers)
    response = requests.post(url, data = post_json, headers=headers)
    #print(response.json())
    return response

@app.route("/preprocess", methods = ['POST'])
def preprocess_api():
    with zipkin_span(
        service_name='iot-preprocessor',
        zipkin_attrs=ZipkinAttrs(
            trace_id=request.headers['X-B3-TraceID'],
            span_id=request.headers['X-B3-SpanID'],
            parent_span_id=request.headers['X-B3-ParentSpanID'],
            flags=1,
            is_sampled=request.headers['X-B3-Sampled'],
        ),
        span_name='preprocessing',
        transport_handler=default_handler,
        port=os.getenv('PREPROCESSOR_PORT', '5001'),
        sample_rate=100,
        encoding=Encoding.V2_JSON
    ):
        start = time.time()
        bytes_received.labels('post','preprocess').inc(len(json.dumps(request.get_json())),exemplar={'trace_id': request.headers['X-B3-TraceId']})
        c.labels('post', '/preprocess').inc(1,exemplar={'trace_id': request.headers['X-B3-TraceID']})
        json_file = analyse(request.get_json())
        json_file = add_parameter_id(json_file)
        print(json_file)
        try:
            store_value(json_file)
        except Exception as e:
            print(e)
            errors.labels('post', '/preprocess').inc(1,exemplar={'trace_id': request.headers['X-B3-TraceID']})
        end = time.time()
        latency.labels('post', '/preprocess').inc(end-start,exemplar={'trace_id': request.headers['X-B3-TraceID']})
    return response

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', threaded=True)
