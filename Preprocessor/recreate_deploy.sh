sudo docker build --network=host -t iot-preprocessor:latest .
sudo docker tag iot-preprocessor:latest $1/iot-preprocessor:latest
sudo docker push $1/iot-preprocessor:latest
kubectl apply -f yaml/iot-preprocessor-configmap.yaml
kubectl delete deploy iot-preprocessor
kubectl apply -f yaml/iot-preprocessor-deployment.yaml
