# distributed-tracing-IoT-app

## Description

The distributed IoT application aims to collect and analyze real-time and historic data streams that are provided by IoT nodes. The application graph is depicted in Figure 6. It consists of a set of components (or microservices) that support the overall application business logic. These components regard:

- Data collection component (IoT Collector): this component is located at the edge part of the infrastructure and is directly connected to the IoT nodes to manage collection and transmission of sensor values. It can be massively replicated according to the needs of the platform (e.g., how many sensors are in-place).
- Data aggregation component (IoT Preprocessor): the data aggregation component is responsible for aggregating the data collected by the IoT Collector and applying basic data management functionalities (e.g., data aggregation, data filtering, outliers removal). It can be replicated according to locality requirements and the number of IoT data streams served at each point of time. It is deployed at the edge part of the infrastructure for guaranteeing latency requirements based on processing of data close to the location of the IoT nodes.
- Backend database (IoT Backend): it regards the database system that stores the data streams for offline analysis, model training etc. This component is a central one for the overall architecture and is deployed in the cloud part of the infrastructure. It can be scaled up and down according to the server workload profile. 
- Data analysis - Forecasting (IoT Predictor): this component is responsible for data analysis over the collected time-series data. It can be scaled according to traffic and data volumes. For the purpose of this analysis, a Long short-term memory (LSTM) model is used for predicting future values of the time-series data.

![alt text](images/IoT_app.png?raw=True)

## Directory contents

The directory of each component contains all the necessary files to build and deploy it. The contents of these directories include:
- The Dockerfile for building the component
- The app/ subdirectory containing all application-related files
- The yaml/ subdirectory containing all the manifests necessary for deploying the component on Kubernetes. This includes:
	- a ConfigMap manifest used for setting up the component's environment
	- a Secrets manifest for including all private information, such as password, authentication tokens etc.
	- a Deployment manifest for generating a new deployment for the component
	- a Service manifest for creating the corresponding service

## Deployment

The code of the application is available in order to build and configure the Docker images from scratch. Additionally, the necessary Kubernetes manifests are used for deploying the application on the cluster. In order to deploy each of the components a series of steps need to be followed which we will describe in this section. For example, for the IoT Collector component:

1. Build the image:
	- sudo docker build --network=host -t iot-collector:latest .

2. Generate a tag for uploading the image on Docker Hub:
	- sudo docker tag io-collector:latest docker-hub-account/iot-collector:latest

3. Push the image on Docker Hub:
	- sudo docker push docker-hub-account/iot-collector:latest

4. Configure ConfigMap:
	- kubectl apply -f yaml/iot-collector-configmap.yaml

5. Configure the component's deployment:
	- kubectl apply -f yaml/iot-collector-deployment.yaml

The same steps apply for all four components of the application, all executed at the corresponding directory. To accelerate the process, the 'recreate\_deploy.sh' script can be used, a script executing all the steps above.

For deploying the database, docker compose is used:
	- sudo docker-compose up -d

## Dependencies

The instrumentation libraries report data to the monitoring systems that collect and store spans metrics and logs. These systems are considered setup in order for the application to be monitored as expected.

## Data Fusion
For the data fusion model the python script 'data_fusion.py' collects the exemplars exported from the IoT application in a given period of time and correlates them with Zipkin and Prometheus data. The JSON data fusion schema is created per exemplar and structured as follows: 
```
[
	{
		"traceID": "exemplar_trace_id",
		"timestamp_end(sec)": "exemplar_timestamp",
		"latency(sec)": "exemplar_value",
		"root_span": {
			"span_id": "span_id",
			"name": "span_name",
			"timestamp(micro)": "begin_timestamp",
			"span_duration(micro)": "span_duration",
			"error": "error message" or "null",
			"throughput_avg": "average_throughput",
			"pods_metrics_avg": [
				{
				"pod": "service_name",
				"cpu": "cpu_usage_rate",
				"memory": "memory_usage"
				},
				...
			]
		},
		"spans": [
			{
				"span_id": "span_id",
				"name": "span_name",
				"timestamp(micro)": "begin_timestamp",
				"span_duration(micro)": "span_duration",
				"error": "error message" or "null",
				"throughput_avg": "average_throughput",
				"pods_metrics_avg": [
					{
					"pod": "service_name",
					"cpu": "cpu_usage_rate",
					"memory": "memory_usage"
					},
					...
				]
			},
			...
		]
	},
	...
]
```
