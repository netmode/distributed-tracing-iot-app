import os
import sys
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
from matplotlib import pyplot as plt
#from fastapi import FastAPI, BackgroundTasks, Header
from flask import Flask, request
import requests
import joblib
from datetime import datetime, timedelta
import json
from py_zipkin.zipkin import zipkin_span, create_http_headers_for_new_span, ZipkinAttrs, Kind, zipkin_client_span
from py_zipkin.request_helpers import create_http_headers
from py_zipkin.encoding import Encoding
from typing import Optional
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from prometheus_client import Counter
from prometheus_client import make_wsgi_app
import threading

DJANGO_TOKEN = os.getenv('BACKEND_AUTH_TOKEN')
DJANGO_URL = 'http://'+os.getenv('BACKEND_HOST', '192.168.2.28')+':'+os.getenv('BACKEND_PORT', '8000')+'/api/'

app = Flask(__name__)
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
    '/metrics': make_wsgi_app()
})

c = Counter('prediction_requests', 'Total requests for predicting future values', ['method', 'endpoint'])
bytes_received = Counter('bytes_received', 'Total bytes received from db', ['method', 'endpoint'])
errors = Counter('errors', 'Errors reported', ['method', 'endpoint'])

def default_handler(encoded_span):
    body = encoded_span
    return requests.post(
        'http://'+os.getenv('ZIPKIN_HOST', '192.168.2.28')+':'+os.getenv('ZIPKIN_PORT', '9411')+'/api/v2/spans',
        data=body,
        headers={'Content-Type': 'application/json'},
    )

@zipkin_client_span(service_name='iot-predictor', span_name='get_data')
def get_data(parameter):
    headers = create_http_headers()
    headers['Authorization'] = 'Token ' + DJANGO_TOKEN
    query = {'parameter': parameter}
    response = requests.get(DJANGO_URL+'sensorvalues', params=query, headers=headers)
    print(response.json())
    print(len(json.dumps(response.json())))
    bytes_received.labels('get', '/sensorvalues').inc(len(json.dumps(response.json())),exemplar={'trace_id': headers['X-B3-TraceId']})
    df = pd.json_normalize(response.json()["data"])[["attributes.value","attributes.timestamp"]]
    print(df)
    return df

@zipkin_client_span(service_name='iot-predictor', span_name='store_prediction')
def store_prediction(timestamp,value,parameter):
    headers = create_http_headers()
    print(headers)
    headers['Authorization'] = 'Token ' + DJANGO_TOKEN
    post_json = {'parameter': parameter, 'value': value, 'timestamp': timestamp}
    print(post_json)
    response = requests.post(DJANGO_URL+'predictions', data=post_json, headers=headers)
    print(response.json())
    return response

def create_time_features(df, target=None):
    """
    Creates time series features from datetime index
    """
    df['date'] = df.index
    df['hour'] = df['date'].dt.hour
    df['dayofweek'] = df['date'].dt.dayofweek
    df['quarter'] = df['date'].dt.quarter
    df['month'] = df['date'].dt.month
    df['year'] = df['date'].dt.year
    df['dayofyear'] = df['date'].dt.dayofyear
    df['sin_day'] = np.sin(df['dayofyear'])
    df['cos_day'] = np.cos(df['dayofyear'])
    df['dayofmonth'] = df['date'].dt.day
    df['weekofyear'] = df['date'].dt.weekofyear
    X = df.drop(['date'], axis=1)
    if target:
        y = df[target]
        X = X.drop([target], axis=1)
        return X, y

    return X

def window_data(X, Y, window=7):
    '''
    The dataset length will be reduced to guarante all samples have the window, so new length will be len(dataset)-window
    '''
    x = []
    y = []
    for i in range(window-1, len(X)):
        x.append(X[i-window+1:i+1])
        y.append(Y[i])
    return np.array(x), np.array(y)

def MSE(actual,predicted):
    return np.mean(np.square(actual-predicted))

def prepare_data(parameter,start,end,interval):
    # We split our dataset to be able to evaluate our models

    #air_pollution = pd.read_csv('air_pollution.csv', parse_dates=['date'])
    #air_pollution.set_index('date', inplace=True)
    #print(air_pollution[:10])
    #air_pollution = air_pollution[['pollution_today']]

    air_pollution = get_data(parameter)
    air_pollution = air_pollution.rename(columns={'attributes.value': 'pollution_today', 'attributes.timestamp': 'date'})
    air_pollution['date'] = pd.to_datetime(air_pollution['date'],format='%Y-%m-%d')
    air_pollution['date'] = air_pollution['date'].astype('datetime64[ns]')
    air_pollution = air_pollution.astype({"pollution_today": float})
    air_pollution.set_index('date', inplace=True)
    
    print(air_pollution.dtypes)
    print(air_pollution[:10])
    
    #split_date = '2014-01-01'
    #df_training = air_pollution.loc[air_pollution.index <= split_date]
    #df_test = air_pollution.loc[air_pollution.index > split_date]
    df_training = air_pollution.iloc[0:int(0.8*len(air_pollution))]
    df_test = air_pollution.iloc[int(0.8*len(air_pollution)):]
    print(f"{len(df_training)} days of training data \n {len(df_test)} days of testing data ")

    #df_training.to_csv('training.csv')
    #df_test.to_csv('test.csv')

    X_train_df, y_train = create_time_features(df_training, target='pollution_today')
    X_test_df, y_test = create_time_features(df_test, target='pollution_today')
    scaler = StandardScaler()
    scaler.fit(X_train_df)  # No cheating, never scale on the training+test!
    dump(scaler, open('scalers/scaler'+str(parameter)+'.pkl', 'wb'))

    X_train = scaler.transform(X_train_df)
    X_test = scaler.transform(X_test_df)

    X_train_df = pd.DataFrame(X_train, columns=X_train_df.columns)
    X_test_df = pd.DataFrame(X_test, columns=X_test_df.columns)

    # For our dl model we will create windows of data that will be feeded into the datasets, for each timestemp T we will append the data from T-7 to T to the Xdata with target Y(t)
    BATCH_SIZE = 64
    BUFFER_SIZE = 100
    WINDOW_LENGTH = 24

    # Since we are doing sliding, we need to join the datasets again of train and test
    X_w = np.concatenate((X_train, X_test))
    y_w = np.concatenate((y_train, y_test))

    X_w, y_w = window_data(X_w, y_w, window=WINDOW_LENGTH)
    X_train_w = X_w[:-len(X_test)]
    y_train_w = y_w[:-len(X_test)]
    X_test_w = X_w[-len(X_test):]
    y_test_w = y_w[-len(X_test):]

    # Check we will have same test set as in the previous models, make sure we didnt screw up on the windowing
    print(f"Test set equal: {np.array_equal(y_test_w,y_test)}")
	
    train_data = tf.data.Dataset.from_tensor_slices((X_train_w, y_train_w))
    train_data = train_data.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

#    val_data = tf.data.Dataset.from_tensor_slices((X_test_w, y_test_w))
#    val_data = val_data.batch(BATCH_SIZE).repeat()
    return X_train_w, X_test_w, train_data, df_test#val_data, df_test

@zipkin_span(service_name='iot-predictor', span_name='prepare_data')
def prepare_train_data(parameter,start,end,interval):
    
    # Create dataframe
    df_training = get_data(parameter)
    df_training = df_training.rename(columns={'attributes.value': 'value', 'attributes.timestamp': 'date'})
    df_training['date'] = pd.to_datetime(df_training['date'],format='%Y-%m-%d')
    df_training['date'] = df_training['date'].astype('datetime64[ns]')
    df_training = df_training.astype({"value": float})
    df_training.set_index('date', inplace=True)
    print(f"{len(df_training)} days of training data")

    # Create X and y
    X_train_df, y_train = create_time_features(df_training, target='value')
    scaler = StandardScaler()
    scaler.fit(X_train_df)  # No cheating, never scale on the training+test!
    #dump(scaler, open('scalers/scaler'+str(parameter)+'.pkl', 'wb'))
    X_train = scaler.transform(X_train_df)
    X_train_df = pd.DataFrame(X_train, columns=X_train_df.columns)

    # Create window data
    # For our dl model we will create windows of data that will be feeded into the datasets, for each timestemp T we will append the data from T-7 to T to the Xdata with target Y(t)
    BATCH_SIZE = 64
    BUFFER_SIZE = 100
    WINDOW_LENGTH = 24
    X_w = X_train
    y_w = y_train
    X_w, y_w = window_data(X_w, y_w, window=WINDOW_LENGTH)
    X_train_w = X_w
    y_train_w = y_w
    train_data = tf.data.Dataset.from_tensor_slices((X_train_w, y_train_w))
    train_data = train_data.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

    return X_train_w, train_data

def create_predict_dataframe(start,end,interval):
    date1 = datetime.strptime(start, '%Y-%m-%d')
    if end is None:
        date2 = date1+timedelta(days=1)
    else:
        date2 = datetime.strptime(end, '%Y-%m-%d')
    dates = [(date1+timedelta(days=i),0) for i in range((date2-date1).days)]
    df = pd.DataFrame(dates,columns=['date','value'])
    df.set_index('date',inplace=True)
    print(df)
    return df

@zipkin_span(service_name='iot-predictor', span_name='prepare_data')
def prepare_predict_data(parameter,start,end,interval):

    # Create dataframe
    df_training = get_data(parameter)
    df_training = df_training.rename(columns={'attributes.value': 'value', 'attributes.timestamp': 'date'})
    df_training['date'] = pd.to_datetime(df_training['date'],format='%Y-%m-%d')
    df_training['date'] = df_training['date'].astype('datetime64[ns]')
    df_training = df_training.astype({"value": float})
    df_training.set_index('date', inplace=True)
    print(f"{len(df_training)} days of training data")

    df_test = create_predict_dataframe(start,end,interval)
    
    X_train_df, y_train = create_time_features(df_training, target='value')
    X_test_df, y_test = create_time_features(df_test, target='value')
    
    scaler = StandardScaler()
    scaler.fit(X_train_df)  # No cheating, never scale on the training+test!
    #dump(scaler, open('scalers/scaler'+str(parameter)+'.pkl', 'wb'))

    X_train = scaler.transform(X_train_df)
    X_test = scaler.transform(X_test_df)
    X_train_df = pd.DataFrame(X_train, columns=X_train_df.columns)
    X_test_df = pd.DataFrame(X_test, columns=X_test_df.columns)

    # For our dl model we will create windows of data that will be feeded into the datasets, for each timestemp T we will append the data from T-7 to T to the Xdata with target Y(t)
    BATCH_SIZE = 64
    BUFFER_SIZE = 100
    WINDOW_LENGTH = 24
    X_w = np.concatenate((X_train, X_test))
    y_w = np.concatenate((y_train, y_test))
    X_w, y_w = window_data(X_w, y_w, window=WINDOW_LENGTH)
    X_train_w = X_w[:-len(X_test)]
    y_train_w = y_w[:-len(X_test)]
    X_test_w = X_w[-len(X_test):]
    y_test_w = y_w[-len(X_test):]

    return X_test_w, df_test

@zipkin_span(service_name='iot-predictor', span_name='train')
def train(X_train_w,train_data,parameter):
    print(X_train_w.shape[-2:])
    dropout = 0.0
    simple_lstm_model = tf.keras.models.Sequential([
        tf.keras.layers.LSTM(
            128, input_shape=X_train_w.shape[-2:], dropout=dropout),
    	    tf.keras.layers.Dense(128),
    	    tf.keras.layers.Dense(128),
    	    tf.keras.layers.Dense(1)
    ])

    simple_lstm_model.compile(optimizer='rmsprop', loss='mae')
	
    EVALUATION_INTERVAL = 200
    EPOCHS = 1

    model_history = simple_lstm_model.fit(
            train_data, epochs=EPOCHS,
            steps_per_epoch=EVALUATION_INTERVAL)#,
#            validation_data=val_data,
#            validation_steps=50)  # ,callbacks=[tensorboard_callback]) #Uncomment this line for tensorboard support

    filename = 'model'+str(parameter)
    simple_lstm_model.save('models/'+filename)

    return simple_lstm_model

@zipkin_span(service_name='iot-predictor', span_name='predict')
def predict(X_test_w, parameter):
    filename = 'model'+str(parameter)
    model = tf.keras.models.load_model('models/'+filename)
    yhat = model.predict(X_test_w).reshape(1, -1)[0]
    yhat = pd.DataFrame(yhat,columns=['value'])
    print(yhat)
    return yhat

def test(X_test_w, df_test, parameter):
    resultsDict = {}
    predictionsDict = {}

    filename = 'model'+str(parameter)
    model = tf.keras.models.load_model('models/'+filename)

    yhat = model.predict(X_test_w).reshape(1, -1)[0]
    #resultsDict['Tensorflow simple LSTM'] = evaluate(y_test, yhat)
    #predictionsDict['Tensorflow simple LSTM'] = yhat
    #print(predictionsDict['Tensorflow simple LSTM'])

    yhat = pd.DataFrame(yhat)
    plt.plot(df_test.pollution_today.values, label='Original')
    print(yhat)
    plt.plot(yhat, color='red', label='LSTM')
    plt.legend()
    #print("MSE: "+str(MSE(df_test.pollution_today.values.to_numpy(),yhat.to_numpy())))

def run_train(parameter,start,end):
    #X_train_w, X_test_w, train_data, df_test = prepare_data(parameter,start,end,1)
    X_train_w, train_data = prepare_train_data(parameter,start,end,1)
    model = train(X_train_w, train_data, parameter) # model return is deprecated and not needed

def run_predict(parameter,start,end,interval):
    X_test_w, df_test = prepare_predict_data(parameter,start,end,interval)
    print(df_test[:10])
    res = predict(X_test_w, parameter)
    dates = [str(pd.to_datetime(x)) for x in df_test["date"]]
    res["date"] = dates
    res.set_index('date', inplace=True)
    print(res)
    return json.loads(res.to_json())

@app.route("/train")
def train_api():
    parameter = request.args.get('parameter')
    start = request.args.get('start')
    end = request.args.get('end')
    with zipkin_span(
        service_name='iot-predictor',
        #zipkin_attrs=ZipkinAttrs(
        #    trace_id = request.headers['X-B3-TraceID'],
        #    span_id = request.headers['X-B3-SpanID'],
        #    parent_span_id = request.headers['X-B3-ParentSpanID'],
        #    flags=1,
        #    is_sampled = request.headers['X-B3-Sampled'],
        #),
        span_name='initiate_training',
        transport_handler=default_handler,
        port=9002,
        sample_rate=100,
        encoding=Encoding.V2_JSON
    ):
        headers = create_http_headers()
        print(headers)
        c.labels('get', '/train').inc(1,exemplar={'trace_id': headers['X-B3-TraceId']})
        #X_train_w, X_test_w, train_data, df_test = prepare_data(parameter,start,end,1)
        X_train_w, train_data = prepare_train_data(parameter,start,end,1)
        model = train(X_train_w, train_data, parameter) # model return is deprecated and not needed
        #background_tasks.add_task(run_train,parameter,start,end)
    return {"train_started": 0}  

@app.route("/predict")
def predict_api():
    parameter = request.args.get('parameter')
    start = request.args.get('start')
    end = request.args.get('end')
    interval = request.args.get('interval')
    with zipkin_span(
        service_name='iot-predictor',
        zipkin_attrs=ZipkinAttrs(
            trace_id = request.headers['X-B3-TraceID'],
            span_id = request.headers['X-B3-SpanID'],
            parent_span_id = request.headers['X-B3-ParentSpanID'],
            flags=1,
            is_sampled = request.headers['X-B3-Sampled'],
        ),
        span_name='generate_prediction',
        transport_handler=default_handler,
        port=int(os.getenv('PREDICTOR_PORT', '9002')),
        sample_rate=100,
        encoding=Encoding.V2_JSON
    ):
        c.labels('get', '/predict').inc(1,exemplar={'trace_id': request.headers['X-B3-TraceID'], 'span_id': request.headers['X-B3-SpanID']})
        X_test_w, df_test = prepare_predict_data(parameter,start,end,interval)
        
        def prediction_task(**kwargs):
            test_window_data = kwargs.get('test_window_data')
            parameter_id = kwargs.get('parameter')
            test_dataframe = kwargs.get('df_test')
            res = predict(test_window_data, parameter_id)
            dates = [pd.to_datetime(x).isoformat() for x in test_dataframe["date"]]
            res["date"] = dates
            print(res)
            store_prediction(dates[0],res.iloc[0]["value"],parameter_id)
        try:
            res = predict(X_test_w, parameter)
            dates = [pd.to_datetime(x).isoformat() for x in df_test["date"]]
            res["date"] = dates
            print(res)
        except Exception as e:
            errors.labels('get', '/predict').inc(1,exemplar={'trace_id': request.headers['X-B3-TraceID'], 'span_id': request.headers['X-B3-SpanID']})
            return e, 500
        try:     
            response = store_prediction(dates[0],res.iloc[0]["value"],parameter)
        #if response.status_code != 200:
        except Exception as e:
            errors.labels('get', '/predict').inc(1,exemplar={'trace_id': request.headers['X-B3-TraceID'], 'span_id': request.headers['X-B3-SpanID']})
            return e, 500
        #thread = threading.Thread(target=prediction_task, kwargs={
        #            'test_window_data': X_test_w,
        #            'parameter': parameter,
        #            'df_test': df_test
        #            })
        #thread.start()
    
    return 'OK', 200


