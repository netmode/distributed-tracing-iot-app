sudo docker build --network=host -t iot-predictor:latest .
sudo docker tag iot-predictor:latest $1/iot-predictor:latest
sudo docker push $1/iot-predictor:latest
kubectl apply -f yaml/iot-predictor-configmap.yaml
kubectl delete deploy iot-predictor
kubectl apply -f yaml/iot-predictor-deployment.yaml
